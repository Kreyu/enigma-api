<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Traits\Scopes\EventTypeScopes;

/**
 * EventType model
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0.1 - 26.03.2018
 */
class EventType extends Model
{
    use EventTypeScopes;

    /**
     * The attributes that are hidden from responses.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
