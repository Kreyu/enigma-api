<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Traits\Scopes\TokenScopes;

/**
 * Token model
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0.1 - 26.03.2018
 */
class Token extends Model
{
    use TokenScopes;

    const TYPE_SERVER = 0;
    const TYPE_CLIENT = 1;

    /**
     * Get related server models
     *
     * @return BelongsToMany
     */
    public function servers(): BelongsToMany
    {
        return $this->belongsToMany(Server::class);
    }

    /**
     * Returns all servers for token with given value
     *
     * @param string $token
     * @return void
     */
    public static function getServers(string $token)
    {
        return static::whereValue($token)->first()
            ->servers()->with('events.type')->get();
    }

    /**
     * Check if given token value exist in database
     *
     * @param string $token
     * @return boolean
     */
    public static function checkExists(string $token = null): bool
    {
        return static::whereValue($token)->exists();
    }
}
