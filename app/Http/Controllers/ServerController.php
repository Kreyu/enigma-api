<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Traits\ApiResponses;
use Illuminate\Http\Request;
use App\EventType;
use App\Server;
use App\Token;
use App\Event;

/**
 * Server Controller
 * Used to process /servers API requests
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0.1 - 26.03.2018
 */
final class ServerController extends ApiController
{
    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * Show all server resources
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $servers = Token::getServers($this->token);

        return $this->response($servers);
    }

    /**
     * Show server resource
     *
     * @param string $guid - Server GUID
     * @return JsonResponse
     */
    public function show(string $guid): JsonResponse
    {
        if (!$server = Server::exists($guid)) {
            return $this->notFoundResponse();
        }
        
        if (!$server->isAuthorized($this->token)) {
            return $this->unathorizedResponse();
        }

        return $this->response($server);
    }

    /**
     * Show server events resources
     *
     * @param string $guid - Server GUID
     * @return JsonResponse
     */
    public function events(string $guid): JsonResponse
    {
        if (!$server = Server::exists($guid)) {
            return $this->notFoundResponse();
        }
        
        if (!$server->isAuthorized($this->token)) {
            return $this->unathorizedResponse();
        }

        $events = $server->events()->get();

        return $this->response($events);
    }

    /**
     * Show five recent server event resources
     *
     * @param string $guid
     * @return JsonResponse
     */
    public function recent(string $guid): JsonResponse
    {
        if (!$server = Server::exists($guid)) {
            return $this->notFoundResponse();
        }
        
        if (!$server->isAuthorized($this->token)) {
            return $this->unathorizedResponse();
        }

        $events = $server->recentEvents($this->quantity)->get();

        return $this->response($events);
    }

    /**
     * Create new event for server with given GUID
     *
     * @param string $guid - Server GUID
     * @return JsonResponse
     */
    public function createEvent(string $guid): JsonResponse
    {
        if (!$server = Server::exists($guid)) {
            return $this->notFoundResponse();
        }
                
        if (!$server->isAuthorized($this->token, true)) {
            return $this->unathorizedResponse();
        }

        $type = EventType::whereName($this->getJson('type'))->first();
        $event = Event::createFromRequest($server, $type);

        return $this->response($event);
    }
}
