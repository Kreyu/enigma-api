<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * Api Controller
 * Used to process all API requests
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0.1 - 26.03.2018
 */
class ApiController extends BaseController
{
    /** @var string */
    protected $token;

    /** @var string */
    protected $quantity;

    /** @var ParameterBag */
    protected $json;

    /**
     * Create a new controller instance.
     *
     * @param Request $request
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token = $request->query->get('token');
        $this->quantity = $request->query->get('quantity') ?? 5;
        $this->json = $request->json();
    }

    /**
     * Gets data from request json parameter bag
     *
     * @param string $key
     * @return mixed
     */
    public function getJson(string $key)
    {
        return $this->json->get($key);
    }

    /**
     * Returns response with HTTP 200
     *
     * @param mixed $data
     * @return JsonResponse
     */
    public function response($data): JsonResponse
    {
        return response()
            ->json([
                'success' => true,
                'status' => 200,
                'message' => 'HTTP_OK',
                'data' => $data
            ], 200);
    }

    /**
     * Returns response with HTTP 401
     *
     * @return JsonResponse
     */
    public function unauthorizedResponse(): JsonResponse
    {
        return response()
            ->json([
                'success' => false,
                'status' => 401,
                'message' => 'HTTP_UNAUTHORIZED'
            ], 401);
    }

    /**
     * Returns response with HTTP 204
     *
     * @return JsonResponse
     */
    public function noContentResponse(): JsonResponse
    {
        return response()
            ->json([
                'success' => false,
                'status' => 204,
                'message' => 'HTTP_NO_CONTENT'
            ], 204);
    }

    /**
     * Returns response with HTTP 404
     *
     * @return JsonResponse
     */
    public function notFoundResponse(): JsonResponse
    {
        return response()
            ->json([
                'success' => false,
                'status' => 404,
                'message' => 'HTTP_NOT_FOUND',
            ], 404);
    }
}
