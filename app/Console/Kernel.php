<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use App\Console\Commands\CreateEvent;
use App\Console\Commands\GenerateToken;
use App\Console\Commands\GenerateGuid;
use App\Console\Commands\ServerAuthenticate;
use App\Console\Commands\StressTest;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateEvent::class,
        GenerateToken::class,
        GenerateGuid::class,
        ServerAuthenticate::class,
        StressTest::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
