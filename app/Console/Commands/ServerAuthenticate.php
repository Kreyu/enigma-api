<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use App\Token;
use App\Server;

/**
 * ServerAuthenticate Command
 * Used to authenticate server with API auth token
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
final class ServerAuthenticate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'server:authenticate {guid} {token}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Authenticates server with given auth token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $server = Server::where('guid', $this->argument('guid'))->first();
        $token = Token::where('value', $this->argument('token'))->first();

        if (!$server) {
            return $this->error('Server with given GUID not found in database.');
        }

        if (!$token) {
            return $this->error('Token with given value not found in database.');
        }

        $status = $server->tokens()->syncWithoutDetaching([$token->id]);
        
        $this->checkSync($status) ? 
            $this->info('Server successfully authenticated.') :
            $this->warn('Server with given GUID already has authentication with given token.');
    }

    /**
     * Check if sync status attached anything
     *
     * @param array $status
     * @return boolean
     */
    private function checkSync(array $status): bool
    {
        return count($status['attached']) > 0;
    }
}