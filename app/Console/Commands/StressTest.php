<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use App\Event;
use App\Token;
use App\Server;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;

/**
 * StressTest Command
 * Used to measure how many time server needs to execute many queries
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
final class StressTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stress:test {--queries=200 : Number of queries to execute}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs queries for stress test';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = microtime(true);
        $quantity = (int) $this->option('queries');

        $query = $this->createQuery();
        for ($i = 0; $i < $quantity; $i++) {
            $query->get();
        }

        $timeElapsed = round(microtime(true) - $start, 3) * 1000; 
        $this->info("Stress test completed. Time elapsed: $timeElapsed ms");
    }

    /**
     * Create query to execute in stress test
     *
     * @return Builder
     */
    private function createQuery(): Builder
    {
        return Event::where('created_at', '>=', Carbon::now()->subMinute())
            ->where('created_at', '<=', Carbon::now());
    }
}