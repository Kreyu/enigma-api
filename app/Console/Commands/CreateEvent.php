<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use App\Event;
use App\Server;
use App\EventType;

/**
 * CreateEvent Command
 * Used to create new event in database
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
final class CreateEvent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:event {guid} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates new event for given GUID server with specified type name';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $server = Server::where('guid', $this->argument('guid'))->first();
        $type = EventType::where('name', $this->argument('type'))->first();

        if (!$server) {
            return $this->error('Server with given GUID not found in database.');
        }

        if (!$type) {
            return $this->error('Event type with given name not found in database.');
        }

        $event = new Event();
        $event->type_id = $type->id;
        $event->server_id = $server->id;

        $event->save() ? 
            $this->info('Event created successfully') : 
            $this->info('Error occurred while creating new event');
    }
}