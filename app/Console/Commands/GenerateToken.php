<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use App\Token;
use Webpatser\Uuid\Uuid;

/**
 * GenerateToken Command
 * Used to generate new API auth token in database
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
final class GenerateToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:token {--type=0 : Specify if token is: 0 for server, 1 for client}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates new API auth token.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $value = $this->generate();
        $token = $this->create($value);

        $this->info('Token ' . $token->value . ' created successfully');
    }

    /**
     * Generate token string
     *
     * @return string
     */
    private function generate(): string
    {
        $token = (string) Uuid::generate();

        if (Token::checkExists($token)) {
            $this->generate();
        }

        return $token;
    }

    /**
     * Create token with generated value in database
     *
     * @return Token
     */
    private function create(string $value): Token
    {
        $token = new Token();
        $token->value = $value;
        $token->type = $this->option('type');
        $token->save();

        return $token;
    }
}