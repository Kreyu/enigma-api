<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Server;
use Webpatser\Uuid\Uuid;

/**
 * GenerateGuid Command
 * Used to generate server GUID
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0.1 - 26.03.2018
 */
final class GenerateGuid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:guid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates server GUID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $guid = $this->generate();

        $this->info('Generated GUID: ' . $guid);
    }

    /**
     * Generate token string
     *
     * @return string
     */
    private function generate(): string
    {
        $guid = (string) Uuid::generate();

        if (Server::checkGuidExists($guid)) {
            return $this->generate();
        }

        return $guid;
    }
}