<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Server model
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0.1 - 26.03.2018
 */
class Server extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'token'
    ];

    /**
     * The attributes that are hidden from responses.
     *
     * @var array
     */
    protected $hidden = [
        'pivot'
    ];

    /**
     * Get authorized api token models
     *
     * @return BelongsToMany
     */
    public function tokens(): BelongsToMany
    {
        return $this->belongsToMany(Token::class)
            ->withTimestamps();
    }

    /**
     * Get related event models
     *
     * @return HasMany
     */
    public function events(): HasMany
    {
        return $this->hasMany(Event::class);
    }

    /**
     * Get newest related event models
     *
     * @param integer $quantity - Specifies how many records to return
     * @return HasMany
     */
    public function recentEvents(int $quantity = 5): HasMany
    {
        return $this->events()
            ->orderBy('created_at', 'DESC')
            ->take($quantity)->with('type');
    }

    /**
     * Check if given server exists
     *
     * @param string $guid - Server GUID
     * @return Server|null
     */
    public static function exists(string $guid): ?Server
    {
        return static::with('events.type')
            ->whereGuid($guid)->first();
    }

    /**
     * Check if server is authorized with given token
     *
     * @param string $token - API auth token
     * @param bool $serverToken - Specifies if token should be server type
     * @return boolean
     */
    public function isAuthorized(string $token, bool $serverToken = false)
    {
        return $this->tokens()
            ->whereValue($token)
            ->when($serverToken, function ($query) {
                return $query->whereType(Token::TYPE_SERVER);
            })->exists();
    }

    /**
     * Check if server with given GUID exist in database
     *
     * @param string $guid - Server GUID
     * @return bool
     */
    public static function checkGuidExists(string $guid): bool
    {
        return static::where('guid', $guid)->exists();
    }
}
