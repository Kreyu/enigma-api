<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Event model
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0.1 - 26.03.2018
 */
class Event extends Model
{
    /**
     * Get related server model
     *
     * @return BelongsTo
     */
    public function server(): BelongsTo
    {
        return $this->belongsTo(Server::class);
    }

    /**
     * Get related event type model
     *
     * @return BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(EventType::class);
    }

    /**
     * Create new event with given server and request data
     *
     * @param Server $server
     * @param EventType $type
     * @return self
     */
    public static function createFromRequest(Server $server, EventType $type): self
    {
        $event = new static();
        $event->server_id = $server->id;
        $event->type_id = $type->id;
        $event->save();

        return $event;
    }
}
