<?php

namespace App\Traits\Scopes;

use Illuminate\Database\Eloquent\Builder;

/**
 * Token scopes trait
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 26.03.2018
 */
trait TokenScopes
{
    public function scopeWhereValue(Builder $builder, string $value): Builder
    {
        return $builder->where('value', $value);
    }

    public function scopeWhereType(Builder $builder, string $value): Builder
    {
        return $builder->where('type', $value);
    }
}
