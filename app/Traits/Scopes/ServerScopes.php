<?php

namespace App\Traits\Scopes;

use Illuminate\Database\Eloquent\Builder;

/**
 * Server scopes trait
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 26.03.2018
 */
trait ServerScopes
{
    public function scopeWhereGuid(Builder $builder, string $guid): Builder
    {
        return $builder->where('guid', $guid);
    }
}