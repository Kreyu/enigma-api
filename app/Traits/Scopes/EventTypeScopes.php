<?php

namespace App\Traits\Scopes;

use Illuminate\Database\Eloquent\Builder;

/**
 * EventType scopes trait
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 26.03.2018
 */
trait EventTypeScopes
{
    public function scopeWhereName(Builder $builder, string $name): Builder
    {
        return $builder->where('name', $name);
    }
}
