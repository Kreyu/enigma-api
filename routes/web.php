<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api', 'middleware' => 'auth'], function () use ($router) {

    $router->get('servers', [
        'as' => 'servers.index',
        'uses' => 'ServerController@index'
    ]);

    $router->get('servers/{guid}', [
        'as' => 'servers.show',
        'uses' => 'ServerController@show'
    ]);

    $router->get('servers/{guid}/events', [
        'as' => 'servers.events',
        'uses' => 'ServerController@events'
    ]);

    $router->get('servers/{guid}/events/recent', [
        'as' => 'servers.events.recent',
        'uses' => 'ServerController@recent'
    ]);

    $router->post('servers/{guid}/events/create', [
        'as' => 'servers.events.create',
        'uses' => 'ServerController@createEvent'
    ]);

});
