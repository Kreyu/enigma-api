# Enigma API

A DayZ game mod API for future usage. Made using [Lumen Micro-Framework](https://lumen.laravel.com/).

## Getting Started

Install packages

```
composer install
```

Start local development server 

```
php -S localhost:8000 -t public
```

Then migrate and seed database

```
php artisan migrate
php artisan db:seed
```

This will create everything we need for development.

## Authentication

All API requests live under `/api` routes. Every request has to be authenticated with token.  
Tokens are stored in database, in `tokens` table.

To authenticate request, add `token` parameter to URL, for example:

```
http://localhost:8000/api/servers/beepboop?token=dev
```

Database seeder creates `dev` token and server with `beepboop` guid, so this request should return `HTTP 200 OK` response.

## Commands

Event creation
```
php artisan create:event <server_guid> <event_name>
```

API auth token generator  
**Type is optional**, and equals 0 on default. **Server type equals 0, client equals 1**.
```
php artisan generate:token --type=<type>
```

Server GUID generator  
```
php artisan generate:guid
```

Server API token authentication
```
php artisan server:authenticate <server_guid> <token_value>
```

Server + database stress test  
Queries (quantity) option equals 200 on default
```
php artisan stress:test --queries=<queries>
```