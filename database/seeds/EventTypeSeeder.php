<?php

use Illuminate\Database\Seeder;
use App\EventType;

/**
 * EventType Seeder
 * Used to create data in EventType table
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
class EventTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EventType::create([
            'name' => 'RAID'
        ]);

        EventType::create([
            'name' => 'EXPIRATION'
        ]);
    }
}
