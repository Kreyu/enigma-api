<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Server;
use App\Token;

/**
 * Server Seeder
 * Used to create data in Server table
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
class ServerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $server = Server::create([
            'guid' => 'beepboop'
        ]);

        $token = Token::first();
        $server->tokens()->attach($token);
    }
}
