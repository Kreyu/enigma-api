<?php

use Illuminate\Database\Seeder;
use App\Token;

/**
 * Token Seeder
 * Used to create data in Token table
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
class TokenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Token::create([
            'value' => 'dev',
            'type' => 0
        ]);
    }
}
