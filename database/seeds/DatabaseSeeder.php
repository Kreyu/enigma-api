<?php

use Illuminate\Database\Seeder;

/**
 * Database Seeder
 * Used to run all other database seeders
 * 
 * @access  public
 * @author  Sebastian Wróblewski <kontakt@swroblewski.pl>
 * @version 1.0 - 25.03.2018
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            TokenSeeder::class,
            ServerSeeder::class,
            EventTypeSeeder::class,
        ]);
    }
}
