<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServerTokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_token', function (Blueprint $table) {
            $table->integer('server_id', false, true);
            $table->foreign('server_id')->references('id')->on('servers')->onDelete('cascade');
            $table->integer('token_id', false, true);
            $table->foreign('token_id')->references('id')->on('tokens')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('server_token');
    }
}
